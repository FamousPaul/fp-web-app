function openMenu() {
    var x = document.getElementById("nav");
    if (x.style.display === "block") {
        x.style.display = "none";
    }   else {
        x.style.display = "block";
    }
}

function showPassword() {
    var x = document.getElementById("pwd");
    var y = document.getElementById("visible");
    if (x.type === "text") {
        x.type = "password";
        y.src = "https://cdn.rebasive.net/fundaprop/eye-off.svg";
    }   else {
        x.type = "text";
        y.src = "https://cdn.rebasive.net/fundaprop/eye.svg";
    }
}

function confirmPassword() {
    var x = document.getElementById("confirm");
    var y = document.getElementById("show");
    if (x.type === "text") {
        x.type = "password";
        y.src = "https://cdn.rebasive.net/fundaprop/eye-off.svg";
    }   else {
        x.type = "text";
        y.src = "https://cdn.rebasive.net/fundaprop/eye.svg";
    }
}


document.querySelectorAll(".drop-zone__input").forEach(inputElement => {
    const dropZoneElement = inputElement.closest(".drop-zone");

    dropZoneElement.addEventListener("click", e => {
        inputElement.click();
    });

    inputElement.addEventListener("change", e => {
        if (inputElement.files.length) {
            updateThumbnail(dropZoneElement, inputElement.files[0])
        }
    })
    dropZoneElement.addEventListener("dragover", e => {
        e.preventDefault();
        dropZoneElement.classList.add("drop-zone__over");
    });

    ["dragleave", "dragend"].forEach(type => {
        dropZoneElement.addEventListener(type, e => {
            dropZoneElement.classList.remove('drop-zone__over');
        });
    });

    dropZoneElement.addEventListener("drop", e => {
        e.preventDefault();

        if (e.dataTransfer.files.length) {
            inputElement.files = e.dataTransfer.files;
            updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
        }

        dropZoneElement.classList.remove("drop-zone__over"); 
    })
});

/**
 * 
 * @param {HTMLELEMT} dropZoneElement 
 * @param {File} file 
*/

function updateThumbnail(dropZoneElement, file) {
    let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

    // Firt time - remove the prompt
    if (dropZoneElement.querySelector(".drop-zone__prompt")) {
        dropZoneElement.querySelector(".drop-zone__prompt").remove();
    }

    // Fist time - there is no thumbnail element, so lets create it
    if (!thumbnailElement) {
        thumbnailElement = document.createElement("div");
        thumbnailElement.classList.add("drop-zone__thumb");
        dropZoneElement.appendChild(thumbnailElement);

    }

    thumbnailElement.dataset.label = file.name;

    // Show thumbnail for image file
    if (file.type.startsWith("image/")) {
        const reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = () => {
            thumbnailElement.style.backgroundImage = `url('${ reader.result }')`;
        }
    } else {
        thumbnailElement.style.backgroundImage = null;
    }
}

var child = document.getElementById('invest-column');
child.style.paddingRight = child.offsetWidth - child.clientWidth + "px";